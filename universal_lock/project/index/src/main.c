#define F_CPU 16000000UL
#include "main.h" 


SemaphoreHandle_t xSem = NULL;

void vVoid( void *pvParameters )
{
  uint8_t a;
  //vSemaphoreCreateBinary(xSem);
  
  while(true)
  {
    xSemaphoreTake( xSem, portMAX_DELAY );
    a = '1';
    a = '2';
    a = '3';
  }
}

int main(void)
{	
  SystemInit();
  initial_clock();
  
  vSemaphoreCreateBinary(xSem);
  //initial_uart();
  //initial_dma();
  
  
  //xTaskCreate(vAnalizator,"Analizator", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 2, NULL);
  //xTaskCreate(vParser,"Parser", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
  
  xTaskCreate(vVoid,"void", configMINIMAL_STACK_SIZE + 2, NULL, tskIDLE_PRIORITY + 1, NULL);
  
  vTaskStartScheduler();
}





void initial_clock( void )
{
  RCC_HCLKConfig(AHB_DEVIDER_VALUE);          //SET APBprescaler
  RCC_PCLK1Config(APB1_DEVIDER_VALUE);        //SET AHB1prescaler
  RCC_PCLK2Config(APB2_DEVIDER_VALUE);        //SET AHB2prescaler
  SystemCoreClockUpdate();                    //CLOCK UPDATE
  return;
}