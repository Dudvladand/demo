#include "main.h"

// ���������� �������. ���� ����� ������� ��� ��������� ������.
#define	ERROR_ACTION(CODE,POS)		do{}while(0)



#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %drn", file, line) */
 
  /* Infinite loop */
  while (1)
  {
  }
}
#endif



void vApplicationTickHook( TaskHandle_t pxTask, char *pcTaskName ){}


void vApplicationMallocFailedHook( TaskHandle_t pxTask, char *pcTaskName ){}


void vApplicationIdleHook( TaskHandle_t pxTask, char *pcTaskName ){}


void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
  ( void ) pcTaskName;
  ( void ) pxTask;
  
  /* Run time stack overflow checking is performed if
  configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
  function is called if a stack overflow is detected. */
  taskDISABLE_INTERRUPTS();
  for( ;; );
}