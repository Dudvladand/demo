#ifndef __CONFIG_H
#define __CONFIG_H

#define STM32L1XX_MD
#define USE_FULL_ASSERT
#define USE_STDPERIPH_DRIVER
#define USE_FREERTOS

#define USART_CONSOLE_RPI_SPEED 38400
#define TIMER_COUNTER 1000              //TIMER COUNTER (default is in mks)
#define AHB_DIVIDER 2
#define APB1_DIVIDER 16
#define APB2_DIVIDER 16


#define RX_BUFFER_SIZE 64
#define TX_BUFFER_SIZE 64


#define BEGIN_COMMAND_SYMBOL '$'       // ( $ ) in ASCII
#define END_COMMAND_SYMBOL 0x0A       // ( \n ) in ASCII
#define COMMAND_SEPARATOR ','
#define CRC_SEPARATOR '*'

#define POLYNOM_CRC8 0xD5  //CRC-8 POLYNOM (getting on wikipedia)


#define BUFFERS_SIZE ( RX_BUFFER_SIZE/2 )  //size buffer of analizator and parser
#define MAX_PARAMETERS 3  //maximum number of parameters in receive functions

//#define COMMAND_LIST_STRING {"func_QQQ","func_WWW","func_EEE",NULL} // ������ ���� (��������� �� ������)
//#define COMMAND_LIST {func_QQQ, func_WWW, func_EEE} // ������� ������� (������ �������)


#define timer_rpi_off_value 10 // in sec








/******************************************************************************/
/*----------------------------AHB DIVIDER SELECT------------------------------*/
/******************************************************************************/

#if AHB_DIVIDER == 1
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div1
#endif
#if AHB_DIVIDER == 2
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div2
#endif
#if AHB_DIVIDER == 4
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div4
#endif
#if AHB_DIVIDER == 8
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div8
#endif
#if AHB_DIVIDER == 16
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div16
#endif
#if AHB_DIVIDER == 64
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div64
#endif
#if AHB_DIVIDER == 128
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div128
#endif
#if AHB_DIVIDER == 256
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div256
#endif
#if AHB_DIVIDER == 512
  #define AHB_DEVIDER_VALUE RCC_SYSCLK_Div512
#endif
#if ( AHB_DIVIDER != 1 ) && ( AHB_DIVIDER != 2 ) &&  \
    ( AHB_DIVIDER != 4 ) && ( AHB_DIVIDER != 8 ) &&  \
    ( AHB_DIVIDER != 16 ) && ( AHB_DIVIDER != 64 ) &&  \
    ( AHB_DIVIDER != 128 ) && ( AHB_DIVIDER != 256 ) &&  \
    ( AHB_DIVIDER != 512 )
  #error "WRONG AHB DIVIDER VALUE"
#endif
/******************************************************************************/
        
        
/******************************************************************************/
/*-------------------------------APB1 DIVIDER SELECT--------------------------*/
/******************************************************************************/ 
      
#if APB1_DIVIDER == 1
  #define APB1_DEVIDER_VALUE RCC_HCLK_Div1
#endif
#if APB1_DIVIDER == 2
  #define APB1_DEVIDER_VALUE RCC_HCLK_Div2
#endif
#if APB1_DIVIDER == 4
  #define APB1_DEVIDER_VALUE RCC_HCLK_Div4
#endif
#if APB1_DIVIDER == 8
  #define APB1_DEVIDER_VALUE RCC_HCLK_Div8
#endif
#if APB1_DIVIDER == 16
  #define APB1_DEVIDER_VALUE RCC_HCLK_Div16
#endif
#if ( APB1_DIVIDER != 1 ) && ( APB1_DIVIDER != 2 ) &&  \
    ( APB1_DIVIDER != 4 ) && ( APB1_DIVIDER != 8 ) &&  \
    ( APB1_DIVIDER != 16 )
  #error "WRONG APB1 DIVIDER VALUE"
#endif
/******************************************************************************/
            
            
/******************************************************************************/
/*-------------------------------APB2 DIVIDER SELECT--------------------------*/
/******************************************************************************/ 
      
#if APB2_DIVIDER == 1
  #define APB2_DEVIDER_VALUE RCC_HCLK_Div1
#endif
#if APB2_DIVIDER == 2
  #define APB2_DEVIDER_VALUE RCC_HCLK_Div2
#endif
#if APB2_DIVIDER == 4
  #define APB2_DEVIDER_VALUE RCC_HCLK_Div4
#endif
#if APB2_DIVIDER == 8
  #define APB2_DEVIDER_VALUE RCC_HCLK_Div8
#endif
#if APB2_DIVIDER == 16
  #define APB2_DEVIDER_VALUE RCC_HCLK_Div16
#endif
#if ( APB2_DIVIDER != 1 ) && ( APB2_DIVIDER != 2 ) &&  \
    ( APB2_DIVIDER != 4 ) && ( APB2_DIVIDER != 8 ) &&  \
    ( APB2_DIVIDER != 16 )
  #error "WRONG APB2 DIVIDER VALUE"
#endif
/******************************************************************************/
      
      
/******************************************************************************/
/*-----------------------------PRESCALERS SETTINGS----------------------------*/
/******************************************************************************/
#define CLOCK_FREQUENCY ( 32000000/AHB_DIVIDER )
#define APB1_TIMERS_INPUT_FREQUENCY ( ( CLOCK_FREQUENCY/APB1_DIVIDER ) * 2 )
#define APB2_TIMERS_INPUT_FREQUENCY ( ( CLOCK_FREQUENCY/APB1_DIVIDER ) * 2 )
/******************************************************************************/

      
      
      
      
      
      
      
#endif /* __CONFIG_H */