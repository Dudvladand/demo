#ifndef __MAIN_H
#define __MAIN_H

//reDEFINE
#define BUFFERS_ANALIZATOR_AND_PARSER_SIZE  ( RX_DMA_BUFFER_SIZE / 2 ) //size buffer of analizator and parser
#define USART_SPEED USART_CONSOLE_RPI_SPEED
#define RX_DMA_BUFFER_SIZE RX_BUFFER_SIZE
#define TX_DMA_BUFFER_SIZE TX_BUFFER_SIZE



#include "stm32l1xx.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"


//DEBUG PORT INCLUDE GROUP
#include "config_command_port_uart_dma.h"
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "console_functions.h"
#include "parser.h"
#include "analizator.h"
//________________________


void initial_clock( void );



#endif /* __MAIN_H */