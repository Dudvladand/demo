#ifndef __PARSER_H
#define __PARSER_H

#include "main.h"


void vParser( void *pvParameters );
bool get_parser_buffer_is_empty( void );
void set_parser_buffer_is_empty( bool value );
void set_parser_buffer_value( unsigned char value, uint8_t index );
void set_protocol_error( void );

#endif /* __PARSER_H */