#ifndef __COMMAND_PORT_UART_H
#define __COMMAND_PORT_UART_H

#include "config_command_port_uart_dma.h"

void initial_uart( void );
//void USART1_IRQHandler( void );
void USART2_IRQHandler( void );

#endif /*    __COMMAND_PORT_UART_H    */