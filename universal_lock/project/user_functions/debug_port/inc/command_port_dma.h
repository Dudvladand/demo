#ifndef __COMMAND_PORT_DMA_H
#define __COMMAND_PORT_DMA_H

#include "config_command_port_uart_dma.h"

void initial_dma( void );
void send_data(uint8_t *value, uint8_t message_size);
uint8_t get_symbol(uint8_t position);
uint16_t get_dma_data_counter( void );

#endif /* __COMMAND_PORT_DMA_H */