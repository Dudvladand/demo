#ifndef __ANALIZATOR_H
#define __ANALIZATOR_H

#define search_mode 0x00
#define search_and_write_mode 0x01

#include "main.h"

void vAnalizator( void *pvParameters );

xSemaphoreHandle getxSemaphoreDebugPortAnalizator( void );
xSemaphoreHandle getxSemaphoreDebugPortParser( void );

#endif /* __ANALIZATOR_H */