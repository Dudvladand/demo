#ifndef __CONFIG_COMMAND_PORT_UART_DMA_H
#define __CONFIG_COMMAND_PORT_UART_DMA_H

#include "main.h"

#include "command_port_uart.h"
#include "command_port_dma.h"


#define USART_GPIO_PORT GPIOA
#define USART_NUM USART2
#define USART_NUM_IRQn USART2_IRQn
#define RX_PIN GPIO_Pin_3
#define TX_PIN GPIO_Pin_2
#define RX_PIN_SOURCE GPIO_PinSource3
#define TX_PIN_SOURCE GPIO_PinSource2
#define GPIO_AF_USART_NUM GPIO_AF_USART2


#define USART_TX_DMA_CHANEL DMA1_Channel7
#define USART_RX_DMA_CHANEL DMA1_Channel6









#endif /*    __CONFIG_COMMAND_PORT_UART_DMA_H    */