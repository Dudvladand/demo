#include "analizator.h"

uint8_t mode = search_mode;

uint8_t analizator_buffer_pointer = 0;
uint8_t analizator_position_pointer = RX_DMA_BUFFER_SIZE;
unsigned char analizator_buffer[ BUFFERS_ANALIZATOR_AND_PARSER_SIZE ];


SemaphoreHandle_t xSemaphoreDebugPortAnalizator = NULL, xSemaphoreDebugPortParser = NULL;

/*xSemaphoreHandle getxSemaphoreDebugPortAnalizator( void ) {
  return xSemaphoreDebugPortAnalizator;
}
xSemaphoreHandle getxSemaphoreDebugPortParser( void ) {
  return xSemaphoreDebugPortParser;
}*/


void vAnalizator( void *pvParameters )
{
  vSemaphoreCreateBinary( xSemaphoreDebugPortAnalizator );
  vSemaphoreCreateBinary( xSemaphoreDebugPortParser );
  
  //vSemaphoreCreateBinary( SemaphoreHandle_t xSemaphore )
  
  //xSemaphoreTake( xSemaphoreDebugPortAnalizator, portMAX_DELAY );
  //xSemaphoreTake( xSemaphoreDebugPortParser, portMAX_DELAY );
  
  uint16_t dma_position_pointer;
  uint8_t symbol;
#ifdef USE_FREERTOS
  while(true) {
    xSemaphoreTake( xSemaphoreDebugPortAnalizator, portMAX_DELAY );
#endif
  dma_position_pointer = get_dma_data_counter();
  while ( analizator_position_pointer != dma_position_pointer)
  {
    if ( ( mode == search_mode ) && ( get_symbol( analizator_position_pointer ) == BEGIN_COMMAND_SYMBOL ) )
    {
      mode = search_and_write_mode;
    }
    else if (mode == search_and_write_mode)
    {
      if ( ( symbol = get_symbol( analizator_position_pointer ) ) == END_COMMAND_SYMBOL )
      {
        mode = search_mode;
        
        if ( analizator_buffer_pointer < BUFFERS_ANALIZATOR_AND_PARSER_SIZE - 1 )
        {
          analizator_buffer[ analizator_buffer_pointer++ ] = 0x00;
          
          if ( get_parser_buffer_is_empty() )
          {
            for ( uint8_t i = 0; i < analizator_buffer_pointer; i++ ) 
            {
              set_parser_buffer_value( analizator_buffer[ i ], i );
              set_parser_buffer_is_empty( false );
            }
          }
          else
          {
            set_protocol_error();
          }
          
        }
        else
        {
          set_protocol_error();
        }
        
        analizator_buffer_pointer = 0;
      }
      else if ( analizator_buffer_pointer < BUFFERS_ANALIZATOR_AND_PARSER_SIZE - 1 )
      {
        analizator_buffer[ analizator_buffer_pointer++ ] = symbol;
      }
      else
      {
        set_protocol_error();
        mode = search_mode;
        analizator_buffer_pointer = 0;
      }
    }
    
    
    if ( analizator_position_pointer == 1 ) analizator_position_pointer = RX_DMA_BUFFER_SIZE + 1;
    analizator_position_pointer--;
  }
  
#ifdef USE_FREERTOS
  xSemaphoreGive( xSemaphoreDebugPortParser );
  }
#else
  return;
#endif
}