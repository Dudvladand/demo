#include "parser.h"

unsigned char parser_buffer[ BUFFERS_ANALIZATOR_AND_PARSER_SIZE ];
bool parser_buffer_is_empty = true;
bool error_status = false;


extern xSemaphoreHandle xSemaphoreDebugPortParser;

void set_protocol_error( void ) 
{
  error_status = true;
}


bool get_parser_buffer_is_empty( void )
{
  return parser_buffer_is_empty;
}



void set_parser_buffer_is_empty( bool value )
{
  parser_buffer_is_empty = value;
  return;
}



void set_parser_buffer_value( unsigned char value, uint8_t index )
{
  parser_buffer[ index ] = value;
  return;
}



uint8_t Crc8( unsigned char *pcBlock, uint8_t len )
{
  unsigned char crc = 0xFF;
  
  while ( len-- )
  {
    crc ^= *pcBlock++;
    for ( uint8_t i = 0; i < 8; i++ ) crc = crc & 0x80 ? ( crc << 1 ) ^ POLYNOM_CRC8 : crc << 1;
  }
  
  return crc;
}


extern xSemaphoreHandle xSemaphoreDebugPortParser;
void vParser( void *pvParameters )
{
#ifdef USE_FREERTOS
  while(true)
  {
    xSemaphoreTake( xSemaphoreDebugPortParser, portMAX_DELAY );
#else//#endif
  //�������� ������
  if ( !parser_buffer_is_empty ) 
  {
#endif
    if ( parser_buffer[0] != 'V' ) 
    {
#ifdef USE_FREERTOS
          continue;
#else
          return;
#endif
    }
    else
    {
      unsigned char command[ MAX_PARAMETERS + 1 ][ BUFFERS_ANALIZATOR_AND_PARSER_SIZE ] = "";
      uint8_t command_pointer = 0;
      uint8_t separator_position = 0;
      uint8_t old_separator_position = 1; //ingnoge 'V' in cutting command
      
#ifdef USE_CRC8
      if ( !strchr( parser_buffer, CRC_SEPARATOR ) ) 
      {
        send_data( "ERR 2", strlen( "ERR 2" ) ); //crc not found
        set_parser_buffer_is_empty( true );
#ifdef USE_FREERTOS
          continue;
#else
          return;
#endif
      }
      else
      {
        unsigned char crc_string[ BUFFERS_ANALIZATOR_AND_PARSER_SIZE ] = "";
        unsigned char crc_value_string [ BUFFERS_ANALIZATOR_AND_PARSER_SIZE ] = "";
        uint8_t crc_value = 0;
        
        
        separator_position = strchr( parser_buffer, CRC_SEPARATOR ) - parser_buffer;
        strncpy( crc_string, parser_buffer, separator_position );
        crc_value = atoi( strncpy( crc_value_string, parser_buffer + separator_position + 1, BUFFERS_ANALIZATOR_AND_PARSER_SIZE - separator_position - 1 ) );
        
        if ( crc_value != Crc8(crc_string, separator_position) )
        {
          send_data( "ERR 0", strlen( "ERR 0" ) ); //crc doesnt match crc8 value
          set_parser_buffer_is_empty( true );
#ifdef USE_FREERTOS
          continue;
#else
          return;
#endif
          
        }
      }
#endif

      //separator_position = 1; //ingnoge 'V' in cutting command
      do
      {
        if ( !strchr( parser_buffer + old_separator_position, COMMAND_SEPARATOR ) )
        {
#ifdef USE_CRC8
          separator_position = strchr( parser_buffer + old_separator_position, CRC_SEPARATOR ) - parser_buffer;
#else
          if ( strchr( parser_buffer + old_separator_position, CRC_SEPARATOR ) )
          {
            separator_position = strchr( parser_buffer + old_separator_position, CRC_SEPARATOR ) - parser_buffer;
          }
          else
          {
            separator_position = BUFFERS_ANALIZATOR_AND_PARSER_SIZE;
          }
#endif
        }
        else
        {
          separator_position = strchr( parser_buffer + old_separator_position, COMMAND_SEPARATOR ) - parser_buffer;
        }
        
        strncpy( command [ command_pointer++ ], parser_buffer + old_separator_position, separator_position - old_separator_position );
        old_separator_position = separator_position + 1;
        
#ifdef USE_CRC8
        if ( separator_position == strchr( parser_buffer + old_separator_position, CRC_SEPARATOR ) ) break;
#else
        if ( strchr( parser_buffer + old_separator_position, CRC_SEPARATOR ) )
        {
          if ( separator_position == ( strchr( parser_buffer + old_separator_position, CRC_SEPARATOR ) - parser_buffer ) ) break;
        }
#endif
        
      } while( separator_position != BUFFERS_ANALIZATOR_AND_PARSER_SIZE );
      
      
      unsigned char answer[ BUFFERS_ANALIZATOR_AND_PARSER_SIZE ] = "";
      char *names[] = COMMAND_LIST_STRING;                                                       // ������ ���� (��������� �� ������)
      void (*pf[])( unsigned char* answer, uint8_t argc, unsigned char argv[][ BUFFERS_ANALIZATOR_AND_PARSER_SIZE ] ) = COMMAND_LIST;  // ������� ������� (������ �������)
      
      //---- ����� ������� �� ����� �� ��������� ������
      for ( uint8_t i=0; names[ i ] != NULL; i++)
      {
        if ( strcmp( names[ i ],command[ 0 ] ) == 0 ) 
        {
          ( *pf[ i ] )( answer, command_pointer, command );
          send_data( answer, strlen( answer ) );
          break;
        }
        if ( names[ i + 1 ] == NULL) send_data( "ERR 404", strlen( "ERR 404" ) ); //command not found 
      }
      
      if ( error_status )
      {
        send_data( "ERR 5", strlen( "ERR 5" ) ); //protocol error
        error_status = false;
      }   
      
      set_parser_buffer_is_empty( true );
    }
  }
//  }//for if(flag)
  
/*#ifndef USE_FREERTOS
  }
#else
  }//��� �������
#endif*/
#ifndef USE_FREERTOS
  return;
#endif
}

