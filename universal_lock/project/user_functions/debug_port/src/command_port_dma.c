#include "command_port_dma.h"

unsigned char USART_DMA_RX_BUFFER[RX_DMA_BUFFER_SIZE];
unsigned char USART_DMA_TX_BUFFER[TX_DMA_BUFFER_SIZE];


void initial_dma( void )
{
  //RESET DMA VALUES
  DMA_DeInit(USART_TX_DMA_CHANEL);
  DMA_DeInit(USART_RX_DMA_CHANEL);
  
  //ENABLE CLOCK ON DMA
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  
  //INITIAL STRUCTURES FOR DMA
  DMA_InitTypeDef DMA_USART_RX, DMA_USART_TX;
  
  //SETTINGS DMA RX
  DMA_USART_RX.DMA_PeripheralBaseAddr = (uint32_t)&(USART_NUM->DR);
  DMA_USART_RX.DMA_MemoryBaseAddr = (uint32_t)&USART_DMA_RX_BUFFER[0];
  DMA_USART_RX.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_USART_RX.DMA_BufferSize = RX_DMA_BUFFER_SIZE;
  DMA_USART_RX.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_USART_RX.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_USART_RX.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_USART_RX.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_USART_RX.DMA_M2M = DMA_M2M_Disable;
  DMA_USART_RX.DMA_Mode = DMA_Mode_Circular;
  DMA_USART_RX.DMA_Priority = DMA_Priority_High;
  
  //SETTINGS DMA TX
  DMA_USART_TX.DMA_PeripheralBaseAddr = (uint32_t)&(USART_NUM->DR);
  DMA_USART_TX.DMA_MemoryBaseAddr = (uint32_t)&USART_DMA_TX_BUFFER[0];
  DMA_USART_TX.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_USART_TX.DMA_BufferSize = TX_DMA_BUFFER_SIZE;
  DMA_USART_TX.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_USART_TX.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_USART_TX.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_USART_TX.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_USART_TX.DMA_M2M = DMA_M2M_Disable;
  DMA_USART_TX.DMA_Mode = DMA_Mode_Normal;
  DMA_USART_TX.DMA_Priority = DMA_Priority_High;
  
  //APPLY SETTINGS DMA
  DMA_Init(USART_TX_DMA_CHANEL, &DMA_USART_TX);
  DMA_Init(USART_RX_DMA_CHANEL, &DMA_USART_RX);
  
  //ENABLE DMA
  DMA_Cmd(USART_TX_DMA_CHANEL, DISABLE);
  DMA_Cmd(USART_RX_DMA_CHANEL, ENABLE);
  
  //ENABLE USART DMA
  USART_DMACmd(USART_NUM, USART_DMAReq_Tx, ENABLE);
  USART_DMACmd(USART_NUM, USART_DMAReq_Rx, ENABLE);
  
  //ENABLE USART INTERRUPT
  //NVIC_EnableIRQ(USART_NUM_IRQn);
  //USART_ITConfig(USART_NUM, USART_IT_TC, ENABLE);
  
  return;
}


void send_data(unsigned char *value, uint8_t message_size)
{
  if ( message_size >= TX_DMA_BUFFER_SIZE ) return; //error!!!
  for ( uint8_t i = 0; i < message_size; i++ ) 
  {
    USART_DMA_TX_BUFFER[ i ] = value[ i ];
  }
  USART_DMA_TX_BUFFER[message_size] = 0x0D; //0x0A in ASCII is LINE FEED (enter)
  /*for ( uint8_t i = message_size + 1; i < TX_DMA_BUFFER_SIZE; i++ ) 
  {
    USART1_DMA_TX_BUFFER[ i ] = 0x00; //0x00 in ASCII is NULL
  }*/
  
  

  //RESET DMA VALUES
  DMA_DeInit(USART_TX_DMA_CHANEL);
  
  //INITIAL STRUCTURES FOR DMA
  DMA_InitTypeDef DMA_USART_TX;
  
  //SETTINGS DMA TX
  DMA_USART_TX.DMA_PeripheralBaseAddr = (uint32_t)&(USART_NUM->DR);
  DMA_USART_TX.DMA_MemoryBaseAddr = (uint32_t)&USART_DMA_TX_BUFFER[0];
  DMA_USART_TX.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_USART_TX.DMA_BufferSize = message_size + 1;
  DMA_USART_TX.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_USART_TX.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_USART_TX.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_USART_TX.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_USART_TX.DMA_M2M = DMA_M2M_Disable;
  DMA_USART_TX.DMA_Mode = DMA_Mode_Normal;
  DMA_USART_TX.DMA_Priority = DMA_Priority_High;
  
  //APPLY SETTINGS DMA
  DMA_Init(USART_TX_DMA_CHANEL, &DMA_USART_TX);
  
  //ENABLE DMA
  DMA_Cmd(USART_TX_DMA_CHANEL, DISABLE);

  
  
  
  while(USART_GetITStatus(USART_NUM, USART_IT_IDLE)){};
  DMA_Cmd(USART_TX_DMA_CHANEL, ENABLE);
  return;
}



uint8_t get_symbol(uint8_t position)
{
  if ( ( position >= 1 ) && ( position <= RX_DMA_BUFFER_SIZE ) ) 
  {
    return USART_DMA_RX_BUFFER[RX_DMA_BUFFER_SIZE - position];
  }
  else
  {
    return 0xFF; //0xFF == -1 in uint8_t
  }
}

uint16_t get_dma_data_counter( void )
{
  return DMA_GetCurrDataCounter(USART_RX_DMA_CHANEL);
}