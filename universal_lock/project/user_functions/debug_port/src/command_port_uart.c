#include "command_port_uart.h"

extern xSemaphoreHandle xSemaphoreDebugPortAnalizator;

void initial_uart( void ) 
{
  //RESET USART VALUES
  USART_DeInit(USART_NUM);
  
  //ENABLE CLOCK ON PORT AND USART
  
  if( USART_NUM == USART1 )
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);/////////////////////////
  }
  else if( USART_NUM == USART2 )
  {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);/////////////////////////
  }
  else if( USART_NUM == USART3 )
  {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
  }
  
  //SET PINS ON ALTERNATE FUNCTIONS (USART)
  GPIO_PinAFConfig(USART_GPIO_PORT, RX_PIN_SOURCE, GPIO_AF_USART_NUM);
  GPIO_PinAFConfig(USART_GPIO_PORT, TX_PIN_SOURCE, GPIO_AF_USART_NUM);
  
  //INITIAL STRUCTURES FOR USART'S PINS
  GPIO_InitTypeDef RXD_RPI, TXD_RPI;
  
  //SETTINGS RX PIN (ON RPI)
  RXD_RPI.GPIO_Pin = GPIO_Pin_3;
  RXD_RPI.GPIO_Mode = GPIO_Mode_AF;
  RXD_RPI.GPIO_Speed = GPIO_Speed_40MHz;
  RXD_RPI.GPIO_OType = GPIO_OType_PP;
  RXD_RPI.GPIO_PuPd = GPIO_PuPd_NOPULL;
  
  //SETTINGS TX PIN (ON RPI)
  TXD_RPI.GPIO_Pin = GPIO_Pin_2;
  TXD_RPI.GPIO_Mode = GPIO_Mode_AF;
  TXD_RPI.GPIO_Speed = GPIO_Speed_40MHz;
  TXD_RPI.GPIO_OType = GPIO_OType_PP;
  TXD_RPI.GPIO_PuPd = GPIO_PuPd_UP;
  
  //APPLY PINS SETTINGS
  GPIO_Init(USART_GPIO_PORT, &RXD_RPI);
  GPIO_Init(USART_GPIO_PORT, &TXD_RPI);
  
  //INITIAL STRUCTURE FOR USART
  USART_InitTypeDef USART_INITIAL_STRUCTURE;
  
  //SETTINGS FOR USART
  USART_INITIAL_STRUCTURE.USART_BaudRate = USART_SPEED;
  USART_INITIAL_STRUCTURE.USART_WordLength = USART_WordLength_8b;
  USART_INITIAL_STRUCTURE.USART_StopBits = USART_StopBits_1;
  USART_INITIAL_STRUCTURE.USART_Parity = USART_Parity_No;
  USART_INITIAL_STRUCTURE.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_INITIAL_STRUCTURE.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  
  //APPLY USART SETTINGS
  USART_Init(USART_NUM, &USART_INITIAL_STRUCTURE);
  
  //ENABLE USART
  USART_Cmd(USART_NUM, ENABLE);
  
  //ENABLE USART INTERRUPT
  USART_ClearITPendingBit(USART_NUM, USART_IT_TC);
  USART_ClearITPendingBit(USART_NUM, USART_IT_RXNE);
  
  NVIC_SetPriority( USART_NUM_IRQn, 2 );
  
  NVIC_EnableIRQ(USART_NUM_IRQn);
  USART_ITConfig(USART_NUM, USART_IT_TC, ENABLE);
  USART_ITConfig(USART_NUM, USART_IT_RXNE, ENABLE);
  
  return;
}


void USART2_IRQHandler( void )
{
  if( USART_GetITStatus( USART_NUM, USART_IT_TC ) )
  {
    USART_ClearITPendingBit(USART_NUM, USART_IT_TC);
    DMA_Cmd(USART_TX_DMA_CHANEL, DISABLE);
    DMA_SetCurrDataCounter(USART_TX_DMA_CHANEL, TX_DMA_BUFFER_SIZE);
  }
  else
  {
#ifdef USE_FREERTOS
    xSemaphoreGiveFromISR( xSemaphoreDebugPortAnalizator, NULL );
#else
    enable_analize_buffer();
#endif
  }
}